# Front-end part of a marketplace

It is a thesis work for diploma

### MVP-1
- Page with authorization
- Page with products
- Description of product
- Product's cart
- Product's order

### Installation
```shell
npm i
```

### Running
```shell
npm start
```
